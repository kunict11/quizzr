// Fisher-Yates algorithm
module.exports.shuffleArray = array => {
    for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        const temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
}

module.exports.arrayEquals = (array1, array2) => {
    return (array1.length==array2.length) && array1.every((x, i) => { return array2[i]===x });
}