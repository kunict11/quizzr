const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const MultipleChoiceQuestionSchema = mongoose.Schema({
    text: {
        type: String,
        required: true
    },
    multiple_correct: {
        type: Boolean,
        default: false,
        required: true
    },
    options: [
        {
            option_text: String,
            correct: Boolean
        }
    ]
});

const MultipleChoiceQuestion = module.exports = mongoose.model('MultipleChoiceQuestion', MultipleChoiceQuestionSchema);

// TODO: Obvious.
const validateQuestion = (question) => {
    return true;
};

module.exports.create = (question) => {
    return new Promise((resolve, reject) => {

        let newQuestion = new MultipleChoiceQuestion(question);
        newQuestion
            .save()
            .then(question => {
                resolve({success: true, status: 201, message: 'Multiple Choice Question created successfully.', question});
            })
            .catch(err => {
                reject({success: false, status: 500, message: 'Something went wrong.'});
            })
    });
};

module.exports.edit = (question) => {
    return new Promise((resolve, reject) => {

        let obj = {"multiple_correct": question.multiple_correct, "text": question.text, "options": question.options};
        console.log(question);
        MultipleChoiceQuestion.findByIdAndUpdate(question._id, obj, {new: true}, function(err, docs){
            if(err){
                reject({success: false, status: 500, message: 'Something went wrong.'});
                return;
            }
            resolve({success: true, status: 200, message: 'Multiple Choice Question updated successfully.', question: docs});
        })
    });
};