const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const GameRoomSchema = mongoose.Schema({
    // TODO: Make unique
    gameroom_name: {
        type: String,
        required: true,
        // unique: true
    },
    author: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    quiz: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Quiz'
    },
    participants: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
        }
    ],
    multiplayer: {
        type: Boolean,
        default: false
    },
    locked: {
        type: Boolean,
        default: false
    },
    game_code: {
        type: Number,
        unique: true
    },
    pin: {
        type: Number
    }
}, {timestamps: true});

module.exports = mongoose.model('GameRoom', GameRoomSchema);