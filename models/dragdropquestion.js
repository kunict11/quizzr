const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const DragDropQuestionSchema = mongoose.Schema({
    text: {
        type: String,
        required: true
    },
    items: [
        {
            type: String
        }
    ]
});

const DragDropQuestion = module.exports = mongoose.model('DragDropQuestion', DragDropQuestionSchema);

module.exports.create = (question) => {
    return new Promise((resolve, reject) => {

        let newQuestion = new DragDropQuestion(question);
        newQuestion
            .save()
            .then(question => {
                resolve({success: true, status: 201, message: 'Drag and Drop Question created successfully.', question});
            })
            .catch(err => {
                reject({success: false, status: 500, message: 'Something went wrong.'});
            })
    });
};

module.exports.edit = (question) => {
    return new Promise((resolve, reject) => {

        let obj = {"text": question.text, "items": question.items};
        DragDropQuestion.findByIdAndUpdate(question._id, obj, {new: true}, function(err, docs){
            if(err){
                reject({success: false, status: 500, message: 'Something went wrong.'});
                return;
            }
            resolve({success: true, status: 200, message: 'Drag & Drop Question updated successfully.', question: docs});
        })
    });
};