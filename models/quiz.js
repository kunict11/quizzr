const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const Question = require('../models/question');
const YesNoQuestion = require('../models/yesnoquestion');
const MultipleChoiceQuestion = require('../models/multiplechoicequestion');
const DragDropQuestion = require('../models/dragdropquestion');

const QuizSchema = mongoose.Schema({
    author: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    name: {
        type: String,
        required: true
    },
    description: {
        type: String
    },
    photo: {
        type: String
    },
    questions: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Question'
        }
    ]
}, 
{
    timestamps: true
});

QuizSchema.pre('remove', async function(next) {
    // delete has to be called on a document to trigger the pre hook
    // TODO https://mongoosejs.com/docs/middleware.html
    let questions = await Question.find({quiz: this._id}).populate('question_data');
    for(let question of questions) {
        await question.delete();
    }
    next();
});

const createFunctions = {
    'YesNoQuestion': YesNoQuestion.create,
    'MultipleChoiceQuestion': MultipleChoiceQuestion.create,
    'DragDropQuestion': DragDropQuestion.create
};

const updateFunctions = {
    'YesNoQuestion': YesNoQuestion.edit,
    'MultipleChoiceQuestion': MultipleChoiceQuestion.edit,
    'DragDropQuestion': DragDropQuestion.edit
};

const Quiz = module.exports = mongoose.model('Quiz', QuizSchema);

module.exports.create = (user_id, name, description, photo) => {
    console.log(`quiz create: ${user_id}`)
    return new Promise((resolve, reject) => {
        let newQuiz = new Quiz({author: user_id, name, description, photo});
        newQuiz.save()
            .then(quiz => {
                resolve({success: true, status: 201, message: 'Quiz successfully created.', quiz});
            })
            .catch(err => {
                reject({success: false, status: 500, message: 'Something went wrong.'});
            });
    });
};

module.exports.addQuestion = async (user_id, quiz_id, question_model, question, time_limit) => {
    return new Promise((resolve, reject) => {
        Quiz
            .findById(quiz_id)
            .populate('questions', '_id sequence_number', null, { sort: { 'sequence_number': -1 } })
            .then(quiz => {
                if (!quiz){
                    reject({success: false, status: 404, message: 'Quiz not found.'});    
                    return;
                }

                if (quiz.author.toString() != user_id.toString()){
                    reject({success: false, status: 400, message: "This quiz doesn't belong to you."});    
                    return;
                }
                
                let creator = createFunctions[question_model];
                if (creator === null || creator === undefined) {
                    reject({success: false, status: 400, message: `Unknown question model: ${question_model}`});
                    return;
                }

                let newSeqId = (quiz.questions.length == 0)? 1: quiz.questions[0].sequence_number + 1;

                creator(question)
                .then(innerQuestion => {
                    let outerQuestion = 
                        new Question(
                            {
                                quiz: quiz_id, 
                                sequence_number: newSeqId,
                                question_data: innerQuestion.question,
                                question_model: question_model,
                                time_limit: time_limit <= 0? 60 : time_limit
                            });
                        outerQuestion
                            .save()
                            .then(q => {
                                quiz.questions.push(q._id)
                                quiz
                                    .save()
                                    .then(updatedRefQuiz => {
                                        resolve({success: true, status:201, message: 'Question successfully added.', question: q})
                                    })
                                    .catch(err => {
                                        reject({success: false, status: 500, message: 'Something went wrong.'});        
                                    })
                                
                            }).catch(err => {
                                reject({success: false, status: 500, message: 'Something went wrong.'});
                            });
                }).catch(err => {
                    reject({success: false, status: err.status, message: err.message});    
                });

            }).catch(err => {
                reject({success: false, status: 500, message: 'Something went wrong.'});
            });            
    });
};


module.exports.editQuestion = async (user_id, question_id, quiz_id, question_model, question_data, time_limit) => {
    time_limit = time_limit <= 0? 60 : time_limit;
    return new Promise((resolve, reject) => {
        console.log(question_id);
        Quiz
            .findById(quiz_id)
            .then(quiz => {
                if (!quiz){
                    reject({success: false, status: 404, message: 'Quiz not found.'});    
                    return;
                }

                if (quiz.author.toString() != user_id.toString()){
                    reject({success: false, status: 400, message: "This quiz doesn't belong to you."});    
                    return;
                }
                
                Question.findById(question_id).populate('question_data').then(q => {
                    if (!q){
                        reject({success: false, status: 404, message: 'Question not found.'});    
                        return;
                    }

                    if(q.question_model === question_model) {
                        let editor = updateFunctions[question_model];
                        if (editor === null || editor === undefined) {
                            reject({success: false, status: 400, message: `Unknown question model: ${question_model}`});
                            return;
                        }

                        editor({...question_data, _id : q.question_data})
                        .then(question_data => {
                            if (q.time_limit != time_limit) {
                                q.updateOne({$set: {time_limit}}, {new: true}).then(_ => {
                                    resolve({success: true, status: 200, message: "Question successfully edited.", question: {...q.toObject(), question_data: question_data.question, time_limit}});
                                }).catch(err => {
                                    reject({success: false, status: 500, message: "Something went wrong."});
                                });
                            } else {
                                resolve({success: true, status: 200, message: "Question successfully edited.", question: {...q.toObject(), question_data: question_data.question}});
                            }
                        })
                        .catch(err => {
                            reject({success: false, status: 500, message: "Something went wrong."});
                        });
                    } else {
                        let creator = createFunctions[question_model];
                        if (creator === null || creator === undefined) {
                            reject({success: false, status: 400, message: `Unknown question model: ${question_model}`});
                            return;
                        }
                        creator(question_data).then(data => {
                            const old_question_data = q.question_data;
                            q.updateOne({$set: {question_model, question_data: data.question._id, time_limit}}, {new: true}).then(_ => {
                                old_question_data.delete().then(_ => {
                                    resolve({success: true, status: 200, message: "Question successfully edited.", question: {...q.toObject(), question_model, question_data: data.question}});
                                }).catch(err => {
                                    reject({success: false, status: 500, message: "Something went wrong."});
                                });;
                            }).catch(err => {
                                reject({success: false, status: 500, message: "Something went wrong."});
                            });
                        }).catch(err => {
                            reject({success: false, status: 500, message: "Something went wrong."});
                        });
                    }
                });

            }).catch(err => {
                console.log(err);
                reject({success: false, status: 500, message: 'Something went wrong.'});
            })
    });
};