import { Component, OnInit, HostListener } from '@angular/core';

import { AuthenticationService } from '../../services/authentication.service';
import { UiService } from '../../services/ui.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

    public dropdownIsHidden: boolean = true;

    constructor(
        public authService: AuthenticationService,
        public uiService: UiService
    ) { }

    ngOnInit(): void { }

    toggleDropdown($event: Event) {
        $event.stopPropagation();
        this.dropdownIsHidden = !this.dropdownIsHidden;
    }

    @HostListener('document:click', ['$event']) clickOutside($event) {
        this.dropdownIsHidden = true;
    }

}
