import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { Quiz } from 'src/app/models/quiz';
import { QuizService } from 'src/app/services/quiz.service';

@Component({
  selector: 'app-edit-quiz',
  templateUrl: './edit-quiz.component.html',
  styleUrls: ['../../form.scss', './edit-quiz.component.scss']
})

export class EditQuizComponent implements OnInit {

  quiz: any = null;

  quizId : string = "";
  questionId : string = "";

  questionType: string = "MultipleChoiceQuestion";
  questionText: string = "";
  options: any = [];
  numOfCorrect : number = null;
  indicesOfCorrect : any = null;
  timeLimit: string = "";
  yesNoAnswer: string = "";

  isUpdate : boolean = false;

  numCorrErrMulStr: string = "Number of correct answers must be less or equal number of options";
  optionErrMulStr: string = "Number of options must be grater then zero";
  corrIndErrMulStr : string = "Bad indices of correct answers";
  numOfOptionsErrDDStr : string = "Drag and drop questions must have at least two options";
  yesNoErrorStr: string = "Only True and False allowed";
  timeLimitErrStr: string = "Incorrect format for time limit";

  optionErrMul : boolean = false;
  numCorrErrMul : boolean = false;
  corrIndErrMul : boolean = false;
  numOfOptionsErrDD : boolean = false;
  yesNoError: boolean = false;
  timeLimitErr: boolean = false;

  constructor(
    private quizService: QuizService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void { 
    this.optionErrMul = false;
    this.numCorrErrMul = false;
    this.corrIndErrMul = false;
    this.numOfOptionsErrDD = false;
    this.yesNoError = false;
    this.timeLimitErr = false;

    this.isUpdate = false;

    this.quizService.getQuiz(this.route.snapshot.params['quiz_id']).subscribe(res => {
      this.quiz = res.quiz;
    })
  }

  onKey(event: Event) {
    this.questionText = (event.target as HTMLInputElement).value;
  }

  clearSelection() {
    this.quizId = "";
    this.questionId = "";

    this.questionType = "MultipleChoiceQuestion";
    this.questionText = "";
    this.options = [];
    this.numOfCorrect = null;
    this.indicesOfCorrect = null;
  
    this.isUpdate = false;
  }

  changeMultipleChoice(question){
    let obj = question["question_data"]["options"];
    this.options = [];
    for(let x of obj){
      this.options.push(x["option_text"]);
    }
    this.options = this.options.join("\n");

    this.numOfCorrect = 0;
    let indOfCorr = [];
    let j = 0;
    for(let x of obj){
      if(x["correct"] == true){
        indOfCorr.push(j)
        this.numOfCorrect += 1;
      }
      j += 1;
    }
    this.indicesOfCorrect = indOfCorr.join(",");
  }

  changeYesNo(question){

    if(question["question_data"]["answer"] == true){
     this.yesNoAnswer = "True";
    }else{
      this.yesNoAnswer = "False";
    }
  }

  changeDragDrop(question){
    let obj = question["question_data"]["items"];
    this.options = obj.join("\n");
  }

  changeQuestion(i: string) {

    this.isUpdate = true;

    let ind = Number.parseInt(i);
    let question = this.quiz.questions[ind];

    this.quizId = question["quiz"];
    this.questionId = question["_id"];

    this.questionType = question["question_model"];
    this.questionText = question["question_data"]["text"];
    
    if(this.questionType == "MultipleChoiceQuestion"){
      this.changeMultipleChoice(question);
    }else if(this.questionType == "YesNoQuestion"){
      this.changeYesNo(question);
    }else{
      this.changeDragDrop(question);
    }

    this.timeLimit = question["time_limit"] || "60";
  }

  submitMultipleChoice(){
    let options = this.options.split("\n");
    let corrIndices = (this.indicesOfCorrect as string).split(",").map(x => +x);

    let min = Number.POSITIVE_INFINITY;
    let max = Number.NEGATIVE_INFINITY;

    for (let item of corrIndices) {
      if(item < min){
        min = item;
      }
    }

    for (let item of corrIndices) {
      if(item > max){
        max = item;
      }
    }
    

    if(corrIndices.length != this.numOfCorrect){
      this.corrIndErrMul = true;
    }
    console.log(`min idx: ${min}, max idx: ${max}`)
    if(min < 0 || max >= options.length){
      this.corrIndErrMul = true;
    }

    let numOfOptions = 0;
    if(this.options != null){
      numOfOptions = options.length;
    }
    
    // Number of correct answers must me less or equal to number of options
    if(numOfOptions < this.numOfCorrect) {
      this.numCorrErrMul = true;
    }

    if(this.optionErrMul || this.numCorrErrMul || this.corrIndErrMul || this.yesNoError){
      return;
    }

    let data = null;

    data = {options: options.map(x => {return {option_text: x, correct: false}}),
      text: this.questionText,
      multiple_correct: corrIndices.length != 1
    };

    for(let index of corrIndices){
      data.options[index].correct = true;
    }

    let question = {quiz_id : this.quiz._id, question_model: this.questionType, question_data: data, time_limit: Number.parseInt(this.timeLimit)};

    return question;
  }

  submitYesNo(){

    if (this.yesNoAnswer.trim().toLowerCase() !== "true" && this.yesNoAnswer.trim().toLowerCase() !== "false") {
      this.yesNoError = true;
      return;
    }
    this.yesNoError = false;
    
    let data = null;

    data = {text: this.questionText,
            answer: this.yesNoAnswer.trim().toLowerCase() === 'true'? 1 : 0
    };

    let question = {quiz_id : this.quiz._id, question_model: this.questionType, question_data: data, time_limit: Number.parseInt(this.timeLimit)};

    return question;
  }

  submitDragDrop(){  
    let options = this.options.split("\n");

    if(options.length < 2){
      this.numOfOptionsErrDD = true;
      return;
    }

    let data = null;

    data = {text: this.questionText, items: options}

    let question = {quiz_id : this.quiz._id, question_model: this.questionType, question_data: data, time_limit: Number.parseInt(this.timeLimit)};

    return question;
  }

  submitQuestion($event: Event) {

    $event.stopPropagation();

    let question = null;

    if (isNaN(Number.parseInt(this.timeLimit)) || Number.parseInt(this.timeLimit) < 0) {
      this.timeLimitErr = true;
      return;
    }
    this.timeLimitErr = false;

    if(this.questionType == "MultipleChoiceQuestion"){
      question = this.submitMultipleChoice();
    }else if(this.questionType == "YesNoQuestion"){
      question = this.submitYesNo();
    }else{
      question = this.submitDragDrop();
    }
    if (!question){
      return;
    }

    if(this.isUpdate == false){
      this.quizService.addQuestion(question).subscribe(res => {
        this.quiz.questions.push(res.question);
        console.log(res.question);
      });
      this.changeQuestion(''+(this.quiz.questions.length-1));
    }else{
      question["question_id"] = this.questionId;

      this.quizService.editQuestion(question).subscribe(res => {
        console.log(res);
        this.quiz.questions[res.question["sequence_number"]-1] = res.question;
      });
    }
  }

  deleteQuestion(event: Event) {
      event.stopPropagation();

      this.quizService.deleteQuestion(this.questionId).subscribe(res => {
        this.quiz.questions = this.quiz.questions
          .filter(q => {return q._id != this.questionId})
          .map((q, i) => {return {...q, sequence_number: i}});
        this.clearSelection();
      });
  }

  deleteQuiz() {
    this.quizService.deleteQuiz(this.quiz._id).subscribe(res => {
        this.router.navigate(['/home'])
    });
  }

  updateType (value) {
    this.questionType = value;
  }

  saveOrder() {
    this.quizService.reorderQuestions(this.quiz._id, this.quiz.questions.map(q => {return q.sequence_number})).subscribe(res => {
      this.quiz.questions = this.quiz.questions.map((q, i) => {return {...q, sequence_number: i+1}});
    });
  }

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.quiz.questions, event.previousIndex, event.currentIndex);
  }
}