import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MakeOpenTComponent } from './make-opent.component';

describe('MakeOpenTComponent', () => {
  let component: MakeOpenTComponent;
  let fixture: ComponentFixture<MakeOpenTComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MakeOpenTComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MakeOpenTComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
