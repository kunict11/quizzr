export const SocketEvents = {
    SINGLEPLAYER_START                         : 'singleplayer_start',
    SINGLEPLAYER_NEXT_QUESTION                 : 'singleplayer_next_question',
    SINGLEPLAYER_ANSWER_QUESTION               : 'singleplayer_answer_question',
    SINGLEPLAYER_FINISHED                      : 'singleplayer_finished',

    GAMEROOM_QUIZ_CREATE                : 'group_quiz_create',
    GAMEROOM_QUIZ_JOIN                  : 'group_quiz_join',
    GAMEROOM_QUIZ_LEAVE                 : 'group_quiz_leave',
    GAMEROOM_QUIZ_CHAT_MSG_SEND         : 'group_quiz_chat_msg_send',
    GAMEROOM_QUIZ_CHAT_MSG_RECV         : 'group_quiz_chat_msg_recv',
    GAMEROOM_QUIZ_CHAT_MSG_META_RECV    : 'group_quiz_chat_msg_meta_recv',
    GAMEROOM_QUIZ_START                 : 'gameroom_quiz_start',
    GAMEROOM_QUIZ_NEXT_QUESTION         : 'gameroom_quiz_next_question',
    GAMEROOM_QUIZ_ANSWER_QUESTION       : 'gameroom_quiz_answer_question',
    GAMEROOM_QUIZ_QUESTION_FEEDBACK     : 'gameroom_quiz_question_feedback',
    GAMEROOM_QUIZ_FINISHED              : 'gameroom_quiz_question_finished',
    GAMEROOM_QUIZ_DESTROYED             : 'gameroom_quiz_destroyed'
}