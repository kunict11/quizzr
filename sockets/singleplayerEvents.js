const Quiz = require('../models/quiz');
const Question = require('../models/question');
const events = require('./eventNames');
const {errorFunction, justSuccessFunction} = require('./socketUtils');
const {decode} = require('html-entities');
const {shuffleArray, arrayEquals} = require('../util/arrayHelper');

const prepareQuestion = (question) => {
    switch(question.question_model) {
        case 'YesNoQuestion':
            break;
        case 'MultipleChoiceQuestion':
            shuffleArray(question.question_data.options);
            break;
        case 'DragDropQuestion':
            question.question_data.items = question.question_data.items.map((item, index) => {return {text: item, index}});
            shuffleArray(question.question_data.items);
            break;
        default:
            console.log('Unknown question type!');
            break;
    }
}

const prepareQuestionForUser = (question) => {
    switch (question.question_model){
        case 'YesNoQuestion':
            return {
                question_data: {
                    text: decode(question.question_data.text),
                },
                question_model: question.question_model,
                time_limit: question.time_limit
            }
        case 'MultipleChoiceQuestion':
            return {
                question_model: question.question_model,
                question_data: {
                    text: decode(question.question_data.text),
                    multiple_correct: question.question_data.multiple_correct,
                    options: question.question_data.options.map(qdb => {
                        return decode(qdb.option_text);
                    })
                },
                time_limit: question.time_limit
            }
        case 'DragDropQuestion':
            return {
                question_model: question.question_model,
                question_data: {
                    text: decode(question.question_data.text),
                    items: question.question_data.items.map(item => {
                        return decode(item.text);
                    })
                },
                time_limit: question.time_limit
            }
        default:
            console.log('Unknown question type!');
            return null;
    }
}

// TODO: This should not be here
// NOTE: Assumes question is populated with question_data
const isCorrect = (question, answer) => {
    switch (question.question_model){
        case 'YesNoQuestion':
            if (question.question_data.answer === answer){
                return {correct: true, message: 'Correct!'};
            }else{
                return {correct: false, correct_answer: question.question_data.answer, message: 'Incorrect!'};
            }
            break;
        case 'MultipleChoiceQuestion':
            // NOTE: You do not send the answer string, you send the INDEX (0 based) of the option
            if (question.question_data.multiple_correct){
                let correct_answer = question.question_data.options.map(option => {return option.correct});
                if (answer!==null && answer!==undefined && arrayEquals(answer, correct_answer)) {
                    return {correct: true, message: 'Correct!'};
                } else {
                    return {correct: false, correct_answer: correct_answer, message: 'Incorrect!'};
                }
            } else {
                // answer and response are indexes, not strings
                if (answer!==null && answer!==undefined && answer>=0 && answer<question.question_data.options.length && question.question_data.options[answer].correct) {
                    return {correct: true, message: 'Correct!'};
                } else {
                    let correct_answer = question.question_data.options.map(opt => {return opt.correct}).indexOf(true);
                    return {correct: false, correct_answer: correct_answer, message: 'Incorrect!'};
                }
            }
            break;
        case 'DragDropQuestion':
            let correct_answer = question.question_data.items.map(item => {return item.index});
            if (answer!==null && answer!==undefined && arrayEquals(answer, correct_answer)) {
                return {correct: true, message: 'Correct!'};
            } else {
                return {correct: false, correct_answer: correct_answer, message: 'Incorrect!'};
            }
            break;
        default:
            console.log(`INVALID (or new?) QUESTION MODEL: ${question.question_model}`);
            return {correct: false, correct_answer: `Invalid question model (did you add a new question?)`, message: 'Eh'};
    }
}

module.exports = (socket) => {

    socket.on(events.SINGLEPLAYER_START, async (data) => {
        const sendError = errorFunction(socket, events.SINGLEPLAYER_START);
        const sendSuccess = justSuccessFunction(socket, events.SINGLEPLAYER_START);

        if (socket.inGame){
            sendError(`You're already in a game.`);
            return;
        }
        if (!data || !data.quiz_id){
            sendError(`You need to provide a quiz id.`);
            return;
        }

        try{
            const desiredQuiz = await Quiz
                                        .findById(data.quiz_id)
                                        .populate({
                                            path: 'questions',
                                            populate: {
                                                path: 'question_data'
                                            }
                                        });

            if (!desiredQuiz){
                sendError(`This quiz doesn't exist`);  
                return;      
            }
            
            socket.inGame = true;
            socket.singleplayer = {};
            socket.singleplayer.quizId = desiredQuiz._id;
            socket.singleplayer.questions = desiredQuiz.toObject().questions.map(questionFromDb => {
                return {
                    _id: questionFromDb._id,
                    sequence_number: questionFromDb.sequence_number,
                    question_model: questionFromDb.question_model,
                    question_data: questionFromDb.question_data,
                    time_limit: questionFromDb.time_limit,
                    has_answered: false,
                    user_answer: null
                };
            });
            
            socket.singleplayer.numCorrect = 0;
            socket.singleplayer.currentQuestionId = -1;
            socket.singleplayer.numOfQuestions = socket.singleplayer.questions.length;
            socket.singleplayer.timeout = null;
            sendSuccess('Welcome to an open trivia game!')
            
        
        }catch (err) {
            console.log(err);
            sendError('Something went wrong #1 / Invalid ID format');        
        }
        
        nextQuestion();
    });
    
    // Answers the last questions that you got with nextQuestion
    socket.on(events.SINGLEPLAYER_ANSWER_QUESTION, async (data) => {
        answerQuestion(data);
    });

    const answerQuestion = (data) => {
        const sendError = errorFunction(socket, events.SINGLEPLAYER_ANSWER_QUESTION);
        if (!socket.inGame ||
            !socket.hasOwnProperty('singleplayer') || 
            socket.singleplayer === null ||
            socket.singleplayer === undefined
            ){
            sendError(`You're not in any open trivia game.`);
            return;
        }

        // ?: This should technically never happen
        if (socket.singleplayer.currentQuestionId == socket.singleplayer.numOfQuestions){
            sendError(`There are no more questions.`)
            socket.singleplayer = null;
            return;
        }

        if (socket.singleplayer.currentQuestionId == -1){
            sendError(`You haven't gotten the question yet.`)
            return;
        }

        if (socket.singleplayer.questions[socket.singleplayer.currentQuestionId].has_answered){
            sendError('You`ve already answered this question!');
            return;
        }

        if (!data.hasOwnProperty('answer')){
            sendError(`You haven't provided an answer.`)
            return;
        }

        currentQuestionId = socket.singleplayer.currentQuestionId;
        currentQuestion = socket.singleplayer.questions[currentQuestionId];

        clearTimeout(socket.singleplayer.timeout);
        socket.singleplayer.questions[socket.singleplayer.currentQuestionId].has_answered = true;
        socket.singleplayer.questions[socket.singleplayer.currentQuestionId].user_answer = data.answer;
        const feedback = isCorrect(currentQuestion, data.answer);
        if (feedback.correct){
            socket.singleplayer.numCorrect++;
        }
        
        socket.emit(events.SINGLEPLAYER_ANSWER_QUESTION, {success:true, feedback});
        
        setTimeout(() => {
            nextQuestion();
        }, 1000); // Question delay, TODO: make variable
    }

    const nextQuestion = () => {
        const sendError = errorFunction(socket, events.SINGLEPLAYER_NEXT_QUESTION);
        // TODO: Too much repetition
        if (!socket.inGame ||
            !socket.hasOwnProperty('singleplayer') || 
            socket.singleplayer === null ||
            socket.singleplayer === undefined
            ){
            sendError(`You're not in any open trivia game.`);
            return;
        }
        
        if (socket.singleplayer.currentQuestionId === socket.singleplayer.numOfQuestions - 1){
            socket.emit(events.SINGLEPLAYER_FINISHED, 
                {
                    success: true,
                    message: `Quizz finished!`,
                    report: {
                         num_correct: socket.singleplayer.numCorrect,
                         num_incorrect: socket.singleplayer.numOfQuestions - socket.singleplayer.numCorrect
                        }});
            socket.singleplayer = null;
            return;
        }

        if (socket.singleplayer.currentQuestionId != -1 &&
            !socket.singleplayer.questions[socket.singleplayer.currentQuestionId].has_answered)
            {
                sendError(`You need to answer your current question before getting a new one.`);
                return;
            }

        let question_to_send = socket.singleplayer.questions[++socket.singleplayer.currentQuestionId];
        prepareQuestion(question_to_send);
        socket.emit(events.SINGLEPLAYER_NEXT_QUESTION, {success:true, question: prepareQuestionForUser(question_to_send)});
        if (question_to_send.time_limit>0) {
            socket.singleplayer.timeout = setTimeout(() => {
                answerQuestion({answer: null});
            }, question_to_send.time_limit * 1000);
        }
    }

}