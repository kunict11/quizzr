const GameRoom = require('../models/gameroom');
const grStorage = require('./gameRoomsStorage');
module.exports.loadGames = async () => {
    GameRoom
        .find({})
        .exec(async (err, res) => {
            if (err){
                console.log(`Error loading games!`);
                console.log(err);
                return;
            }
            console.log(`Loading games into storage.`);
            for (const gameRoomDb of res){
                console.log(`\t ...loading`)
                grStorage.setGameRoom(gameRoomDb._id.toString(), await docToStorage(gameRoomDb));
            }
            console.log(`All games loaded from database.`);
        });

}

module.exports.loadGame = async (gameroom) => {
    grStorage.setGameRoom(gameroom._id.toString(), await docToStorage(gameroom));
    console.log(`Gameroom loaded`);
};

const docToStorage = async (gameroomDoc) => {
    const tag = `[Gr Manager: docToStorage]`
    if (!gameroomDoc.populated('participants')){
        console.log(`\t\t${tag} ...populating participants`);
        gameroomDoc = await gameroomDoc.populate('participants', '_id username').execPopulate();
    }
    if (!gameroomDoc.populated('author')){
        console.log(`\t\t${tag} ...populating author`);
        gameroomDoc = await gameroomDoc.populate('author', '_id username').execPopulate();
    }
    if (!gameroomDoc.populated('quiz')){
        console.log(`\t\t${tag} ...populating quiz`);
        gameroomDoc = await gameroomDoc.populate({
            path: 'quiz',
            populate: {
                path: 'questions',
                populate: {
                    path: 'question_data'
                }
            }
        }).execPopulate();
    }

    return {
        gameroom_id:        gameroomDoc._id.toString(),
        gameroom_name:      gameroomDoc.gameroom_name,
        author:             gameroomDoc.author,
        participants:       gameroomDoc.participants.map(p => {return {id: p._id.toString(), username: p.username}}),
        quiz:               gameroomDoc.toObject().quiz,
        multiplayer:        gameroomDoc.multiplayer,
        locked:             gameroomDoc.locked,
        game_code:          gameroomDoc.game_code,
        pin:                gameroomDoc.pin,
        state:              grStorage.GAMEROOM_STATE.IDLE
    }
}

