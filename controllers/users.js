const multer = require('multer');
const path = require('path');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');

const auth_config = require('../config/auth.config');
const error = require('../util/error');
const User = require('../models/user');

const photo_path = 'uploads';
const multerStorage = multer.diskStorage({
    destination: path.join('public', photo_path),
    filename: function(req, file, callback){
        callback(null, req.user.username + '.' + file.mimetype.split('/')[1]);
    }
});

const upload = multer({
    storage: multerStorage,
    fileFilter: function(req, file, callback){
        const filetypes = /jpeg|jpg|png/;
        const extname = filetypes.test(file.mimetype.split('/')[1].toLowerCase());
        const mimetype = filetypes.test(file.mimetype);

        if(extname && mimetype){
            callback(null, true);
        } else {
            callback('Images only');
        }
    }
}).single('avatar');

const findByUsername = username => {
    return User.findOne({ username: username }).exec();
}

const encryptPassword = password => {
    return new Promise((resolve, reject) => {
        bcrypt.genSalt(10, (err, salt) => {
            if(err) {
                reject(err);
            } else {
                bcrypt.hash(password, salt, (err, hash) => {
                    if(err) {
                        reject(err);
                    } else {
                        resolve(hash);
                    }
                });
            }
        });
    });
}

const comparePassword = (candidatePassword, currentHash) => {
    return new Promise((resolve, reject) => {
        bcrypt.compare(candidatePassword, currentHash, (err, isMatch) => {
            if(err) {
                return reject(err);
            } else {
                resolve(isMatch);
            }
        });
    });
}

const sign_jwt = id => {
    return jwt.sign(
        { user_id: id }, 
        auth_config.jwt_secret,
        { expiresIn: '7d' }
    );
}

module.exports.uploadPhoto = async (req, res, next) => {
    try {
        let user = await User.findById(req.user.id);
        if(!user) {
            next(error(404, 'User not found'));
        } else {
            upload(req, res, async (err) => {
                if(err) {
                    next(err);
                } else if(req.file == undefined){
                    next(error(400, 'No file selected'));
                } else {
                    user.photo = path.join(photo_path, req.file.filename);
                    await user.save();
                    res.status(200).json({ success: true, message: 'Photo uploaded' });
                }
            });
        }
    } catch (err) {
        next(err);
    }
}

module.exports.changePassword = async (req, res, next) => {
    try {
        let user = await User.findById(req.user.id);
        if(!user) {
            next(error(404, 'User not found'));
        } else {
            const isMatch = await comparePassword(req.body.old_password, user.password);
            if (!isMatch) {
                next(error(401, 'Wrong password'));
            } else {
                if(req.body.old_password === req.body.new_password) {
                    next(error(400, 'New and old password are same!'));
                } else {
                    const password = await encryptPassword(req.body.new_password);
                    user.password = password;
                    user = await user.save();
                    res.status(200).json({ success: true, message: 'Password changed' });
                }
            }
        }
    } catch (err) {
        next(err);
    }
}

module.exports.register = async (req, res, next) => {
    try {
        const new_user = new User({
            username: req.body.username,
            password: req.body.password
        });
        if(!new_user.username) {
            next(error(400, 'You must provide a username'));
        } else if(!new_user.password) {
            next(error(400, 'You must provide a password'));
        } else {
            let user = await findByUsername(new_user.username);
            if(user) {
                next(error(400, 'Username already taken'));
            } else {
                const password = await encryptPassword(new_user.password);
                new_user.password = password;
                user = await new_user.save();
                res.status(201).json({
                    sucess: true,
                    message: 'User registered',
                    token: 'bearer ' + sign_jwt(user._id),
                    user: { id: user._id, username: user.username, photo: user.photo },
                });
            }
        }
    } catch (err) {
        next(err);
    }
}

module.exports.login = async (req, res, next) => {
    try {
        const username = req.body.username;
        const password = req.body.password;
        if(!username) {
            next(error(400, 'You must provide a username'));
        } else if(!password) {
            next(error(400, 'You must provide a password'));
        } else {
            const user = await findByUsername(username);
            if(!user) {
                next(error(401, 'Invalid credentials'));
            } else {
                const isMatch = await comparePassword(password, user.password);
                if(!isMatch) {
                    next(error(401, 'Invalid credentials'));
                } else {
                    res.status(200).json({
                        success: true,
                        message: "Logged in",
                        token: 'bearer ' + sign_jwt(user._id),
                        user: {
                            id: user._id,
                            username: user.username,
                            photo: user.photo
                        }
                    });
                }
            }
        }
    } catch (err) {
        next(err);
    }
}

module.exports.getCurrentUser = (req, res) => {
    res.status(200).json({ success: true, user: req.user });
}