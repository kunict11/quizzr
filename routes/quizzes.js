const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');
const axios = require('axios');

const Quiz = require('../models/quiz')
const Question = require('../models/question');
const GameRoom = require('../models/gameroom');
const openTrivia = require('../util/openTrivia');
const grLoader = require('../sockets/gameRoomLoader');
const error = require('../util/error');
const { decodeQuestion } = require('../util/questionHelper');

router.get('/openTrivia/categories', (req, res) => {
    res.status(200).json({success: true, categories: openTrivia.getCategories()});
});

router.get('/openTrivia/category/:id', (req, res) => {
    const categoryName = openTrivia.getCategoryName(req.params.id);
    if(!categoryName) {
        res.status(400).json({ success: false, message: "Category not found" });
    } else {
        res.status(200).json({ success: true, categoryName });
    }
});

router.get('/get/:id', auth.required, async (req, res, next) => {
    try {
        const quiz = await Quiz.findById(req.params.id).populate({
            path: 'questions',
            populate: { path: 'question_data' }
        }).exec();

        if(!quiz) {
            next(error(404, "Quiz not found"));;
        } else {
            res.status(200).json({
                success: true, 
                quiz: {...quiz.toObject(), questions: quiz.toObject().questions.map(question => {return decodeQuestion(question)})}
            });
        }
    } catch(err) {
        next(err);
    }
});

router.get('/user', auth.required, async (req, res, next) => {
    try {
        const quizzes = await Quiz.find({ author: req.user.id }).sort({createdAt : -1}).exec();
        res.status(200).json({success: true, quizzes});
    } catch(err) {
        next(err);
    }
});

router.post('/create', auth.required, (req, res) => {
    //TODO: Validation
    const user_id = req.user.id;
    Quiz
        .create(user_id, req.body.name, req.body.description, req.body.photo)
        .then(response => {
            res.status(response.status).json(response);
        })
        .catch(err => {
            res.status(err.status).json(err);
        });
});

router.post('/createOpenTrivia', auth.required, async (req, res) => {
    let openTriviaAuthor = await openTrivia.getOpenTriviaAuthor();
    console.log(`Author: ${openTriviaAuthor}`)

    const link = openTrivia.getUrl(req.body.category, req.body.amount);

    axios.get(link).then(async result => {
        if (result.data.response_code === 0) {
            Quiz.create(openTriviaAuthor, "Open Trivia").then(async quiz => {
                quiz = quiz.quiz;
                for(let question of result.data.results) {
                    let model;
                    let q;
                    switch(question.type) {
                        case 'multiple':
                            model = 'MultipleChoiceQuestion';
                            let options = [
                                {option_text: question.correct_answer, correct: true}
                            ].concat(question.incorrect_answers.map(x => {return {option_text: x, correct: false}}));
                            q = {
                                text: question.question,
                                multiple_correct: false,
                                options
                            };
                            break;
                        case 'boolean':
                            model = 'YesNoQuestion';
                            q = {
                                text: question.question,
                                answer: question.correct_answer === 'True'
                            };
                            break;
                    }
                    try {
                        await Quiz.addQuestion(openTriviaAuthor, quiz._id, model, q, openTrivia.timeLimit);
                    } catch(err) {
                        return res.status(500).json({ success: false, message: "Something went wrong" });
                    }
                }
                return res.status(200).json({success: true, quiz});
            }).catch(err => {
                return res.status(500).json({ success: false, message: "Something went wrong" });
            })
        } else {
            return res.status(500).json({ success: false, message: "Something went wrong" });
        }
    }).catch(err => {
        return res.status(500).json({ success: false, message: "Something went wrong" });
    });
});

router.post('/add-question', auth.required, (req, res) => {
    let question_data  = req.body.question_data;
    let question_model = req.body.question_model;
    let quiz_id        = req.body.quiz_id;
    let user_id        = req.user.id;
    let time_limit     = req.body.time_limit;

    Quiz
        .addQuestion(user_id, quiz_id, question_model, question_data, time_limit)
        .then(response => {
            res.status(response.status).json(response);
        }).catch(err => {
            res.status(err.status).json(err);
        });
});

const generateCode = () => {
    let pin = Math.floor(Math.random()*100000);
    pin += 1000000*Math.floor(1 + Math.random()*8);
    return pin;
}

const generateUniqueGameCode = async () => {
    const game_rooms = await GameRoom.find({}, 'game_code');
    const game_room_codes = game_rooms.map(gr => gr.game_code);

    let code = undefined;
    while (true){
        code = generateCode();
        if (!game_room_codes.includes(code)){
            return code;
        }
    }
}

// TODO: Validate, check for errors better
router.post('/create-gameroom', auth.required, async (req, res) => {
    let gameroom_name = req.body.gameroom_name;
    let author_id = req.user.id;
    let quiz_id = req.body.quiz_id;
    let multiplayer = req.body.multiplayer;
    let locked = req.body.locked;

    let pin = undefined;
    let game_code = await generateUniqueGameCode();
    if (locked){
        pin = generateCode();
    }

    let newGameRoom = new GameRoom({gameroom_name, author: author_id, quiz: quiz_id, participants:[], multiplayer, locked, pin, game_code});
    
    newGameRoom.save()
               .then(async gr => {
                    await grLoader.loadGame(newGameRoom);

                    res.status(200).json({success: true,
                                         message: `Game room successfully created.`, 
                                         gameroom_id: gr._id,
                                         multiplayer,
                                         locked,
                                         pin,
                                         game_code});
               }).catch(err => {
                   console.log(err);
                   res.status(500).json({success: false, message: `Something went wrong.`});
               });
});

router.get('/gamerooms', auth.required, async (req, res, next) => {
    try {
        const quizzes = await GameRoom.find({multiplayer: true, locked: false}).sort({createdAt : -1}).exec();
        res.status(200).json(quizzes);
    } catch (err) {
        next(err);
    }
});

router.get('/gamerooms/:id', auth.required, async (req, res, next) => {
    try {
        const gameroom = await GameRoom.findById(req.params.id)
        .populate('participants', {'username': 1, 'photo': 1})
        .populate('author', {'username': 1, 'photo': 1})
        .populate({
            path: 'quiz',
            populate: {
                path: 'questions',
                populate: {
                    path: 'question_data'
                }
            },
            populate: {
                path: 'author',
                select: 'username'
            }
        })
        .exec();
        res.status(200).json(gameroom);
    } catch (err) {
        next(err);
    }
});

router.post('/edit-question', auth.required, (req, res) => {
    let question_id    = req.body.question_id;
    let quiz_id        = req.body.quiz_id;
    let question_model = req.body.question_model;
    let question_data  = req.body.question_data;
    let time_limit     = req.body.time_limit;
    let user_id        = req.user.id;

    Quiz
        .editQuestion(user_id, question_id, quiz_id, question_model, question_data, time_limit)
        .then(response => {
            res.status(response.status).json(response);
        }).catch(err => {
            res.status(err.status).json(err);
        });
});

router.post('/reorder-questions', auth.required, (req, res) => {
    const quiz_id = req.body.quiz_id;
    const question_indexes = req.body.questions;
    let user_id = req.user.id;

    Quiz.findById(quiz_id).then(async quiz => {
        if(!quiz) {
            res.status(404).json({success: false, message: 'Quiz not found'});
        } else {
            // TODO Validate question order
            if(quiz.author != user_id) {
                res.status(401).json({success: false, message: 'This quiz doesn\'t belong to you.'});
            } else if(quiz.questions.length != question_indexes.length) {
                res.status(400).json({success: false, message: 'Invalid ordering'});
            } else {
                let questions = quiz.toObject().questions;
                // TODO Remove sequence numbers
                // TODO https://docs.mongodb.com/manual/reference/method/db.collection.bulkWrite/
                for(const [i,q] of question_indexes.entries()) {
                    if(i+1==q)
                        continue;
                    await Question.findByIdAndUpdate(questions[q-1], {$set: {sequence_number: i+questions.length+1}}).exec();
                }
                for(const [i,q] of question_indexes.entries()) {
                    if(i+1==q)
                        continue;
                    await Question.findByIdAndUpdate(questions[q-1], {$set: {sequence_number: i+1}}).exec();
                }
                // await Question.bulkWrite(question_indexes.map((q, i) => {
                //     return { updateOne: {filter: {_id: questions[i]}, update: {$set: {sequence_number: q}}}}
                // }), {bypassDocumentValidation: true});

                const new_order = question_indexes.map(i => {return quiz.questions[i-1]});
                await quiz.updateOne({$set: {questions: new_order}});
                res.status(200).json({success:true, quiz: {...quiz.toObject(), questions: new_order}});
            }
        }
    });
});

router.post('/delete-question', auth.required, (req, res) => {
    const question_id = req.body.question_id;
    let user_id = req.user.id;

    Question.findById(question_id).populate('quiz question_data').then(async question => {
        if(!question) {
            res.status(404).json({success: false, message: 'Question not found'});
        } else {
            if(question.quiz.author != user_id) {
                res.status(401).json({success: false, message: 'This quiz doesn\'t belong to you.'});
            } else {
                let quiz = question.quiz;
                await question.delete();

                let questions = question.quiz.toObject().questions.filter(q => {return q != question_id});
                await quiz.updateOne({$set: {questions: questions}});
                res.status(200).json({success:true, message: 'Question deleted'});
            }
        }
    });
});

router.post('/delete', auth.required, (req, res) => {
    const quiz_id = req.body.quiz_id;
    let user_id = req.user.id;

    Quiz.findById(quiz_id).populate({ path: 'questions', populate: { path: 'question_data' }}).then(async quiz => {
        if(!quiz) {
            res.status(404).json({success: false, message: 'Quiz not found'});
        } else {
            if(quiz.author != user_id) {
                res.status(401).json({success: false, message: 'This quiz doesn\'t belong to you.'});
            } else {
                await quiz.delete();
                res.status(200).json({success:true, message: 'Quiz deleted'});
            }
        }
    });
});

router.post('/gamerooms', auth.required, async (req, res, next) => {
    const game_code = req.body.game_code;
    try {
        const gameroom = await GameRoom.findOne({game_code: game_code}).exec();

        if (!gameroom) {
            next(error(404, 'Gameroom not found'));
        }
        else {
            res.status(200).json({success: true, id: gameroom._id});
        }
    } catch (err) {
        next(err);
    }
});

router.get('/latest', async (req, res, next) => {
    try {
        const quizzes = await Quiz.find({author: {$ne: await openTrivia.getOpenTriviaAuthor()}}).sort({createdAt : -1}).select('_id name').limit(10).exec();
        res.status(200).json(quizzes);
    } catch (err) {
        next(err);
    }
});

router.post('/find', async (req, res, next) => {
    try {
        const query = req.body.query;
        const quizzes = await Quiz
            .find({author: {$ne: await openTrivia.getOpenTriviaAuthor()}})
            .and(query.split(' ').map(s=>{ return {name: {$regex: `(?i).*${s}.*`}}}))
            // .find({$text: {$search: query}})
            .sort({createdAt : -1})
            .select('_id name')
            .limit(10)
            .exec();
        res.status(200).json(quizzes);
    } catch (err) {
        next(err);
    }
});

router.get('/details/:quiz_id', async (req, res, next) => {
    try {
        const quiz = await Quiz.findById(req.params.quiz_id).populate('author', '_id username photo').exec();
        res.status(200).json({
            _id: quiz._id,
            name: quiz.name,
            details: quiz.details,
            author: quiz.author,
            question_number: quiz.questions.length
        });
    } catch (err) {
        next(err);
    }
});

module.exports = router;